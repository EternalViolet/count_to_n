# Count to N

Just prints numbers up from 0 to n inclusive.
Wanted to get more familiar with rust and also do some timing tests. :)

# Run
`cargo run`

# Build + run
`cargo build`  
`./target/debug/count_to_n`

# Test
`cargo test`
