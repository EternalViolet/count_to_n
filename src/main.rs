use std::env;
use std::time::Instant;
mod iterative;
mod recursive;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        print_usage();
        panic!("Error: Must supply positive integer N");
    }
    let numb_to_count_to = args[1].parse::<u32>().expect("Error: Given N must be a positive integer");

    println!("Start");

    let mut now = Instant::now();
    iterative::iterative_count(numb_to_count_to);
    let iterative_time = now.elapsed().as_millis();

    now = Instant::now();
    recursive::recursive(numb_to_count_to);
    let recursive_time = now.elapsed().as_millis();
    
    println!("End");

    println!("Iterative took: {} ms", iterative_time);
    println!("Recursive took: {} ms", recursive_time);
}

fn print_usage() {
    println!("
        USAGE: 
            count_to_n [N]
        
        N:
            A positive integer that will be counted to, inclusive.

        Example:
            count_to_n 10"
    );
}
