pub fn iterative_count(n: u32) {
    for i in 0..n + 1 {
        println!("{}", i);
    }

    // Or another way.
    // (0..n+1).for_each (|i| {
    //     println!("{}", i);
    // });
}
