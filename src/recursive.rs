pub fn recursive(n: u32) {
    recursive_print(0, n);
}

fn recursive_print(current: u32, til: u32) {
    println!("{}", current);
    if current != til {
        recursive_print(current + 1, til);
    }
}
